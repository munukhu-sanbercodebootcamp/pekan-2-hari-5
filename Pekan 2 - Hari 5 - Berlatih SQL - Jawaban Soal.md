## Soal 1 Membuat Database
**Jawaban Soal 1**

```
CREATE DATABASE myshop
```

## Soal 2 Membuat Table di Dalam Database
**Jawaban Soal 2**

```
CREATE TABLE users(
	id INT AUTO_INCREMENT PRIMARY KEY,
	name varchar(255),
	email varchar(255),
	password varchar(255)
)
CREATE TABLE items(
	id INT AUTO_INCREMENT PRIMARY KEY,
	name varchar(255),
	description varchar(255),
	price int,
    stock int,
    category_id int,
    FOREIGN KEY (category_id) REFERENCES categories(id)
)
CREATE TABLE categories(
	id int AUTO_INCREMENT primary key,
    name varchar(255)
)
```

## Soal 3 Memasukkan Data pada Table
**Jawaban Soal 2**

```
insert into users(name,email,password)
VALUES ('John Doe','john@doe.com','john123'),
('John Doe','jane@doe.com','jenita123')
insert into categories(name)
VALUES ('gadget'),('cloth'),('men'),('women'),('branded')
insert into items(name,description,price,stock,category_id)
VALUES ('Sumsang b50','hape keren dari merek sumsang',4000000,100,1),
('Uniklooh','baju keren dari brand ternama',500000,50,2),
('IMHO Watch','tam tangan anak yang jujur banget',2000000,10,1)
```

## Soal 4 Mengambil Data dari Database
**Jawaban Soal 4a**

```
select id,name,email from users
```

**Jawaban Soal 4b1**

```
select * from items where price > 1000000
```

**Jawaban Soal 4b2**

```
select * from items where name like '%sang%'
```

**Jawaban Soal 4c**

```
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori
FROM items
inner JOIN categories
ON items.category_id = categories.id
```

## Soal 5 Mengubah Data dari Database

**Jawaban Soal 2**

```
update items set price = 2500000 where items.name like '%Sumsang%'
```
